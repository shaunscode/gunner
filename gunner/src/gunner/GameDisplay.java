package gunner;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JPanel;

import gunner.bases.Constants;
import gunner.bases.Entity;
import gunner.things.MapCorner;
import gunner.things.MapEdge;
import gunner.utils.Vec2;

/**
 *
 * @author SAC248
 */
public class GameDisplay extends JPanel {

	private GameState state;
	private Color playerColor = new Color(0, 128, 255);
	private Color playerPointer = new Color(255, 64, 64);
	private Color blockColor = new Color(0, 128, 128);
	private ArrayList<MapCorner> cs;
	private static Vec2 playerScreenPosition = new Vec2();

	public GameDisplay(GameState state) {
		this.state = state;
		cs = state.getMap().getCorners();
	}

	int frameRate = 0;
	int frameCount = 0;
	long loopTick = 0;
	long frameTick = 0;

	long currentTime = 0;
	long nextTime = System.currentTimeMillis();

	public void repaint(long loopTick, long frameTick) {

		super.repaint();
		frameCount++;
		currentTime = System.currentTimeMillis();
		while (currentTime - nextTime > 1000) {
			nextTime += 1000;
			frameRate = frameCount;
			frameCount = 0;
		}
	}

	public void paintComponent(Graphics g) {

		g.setColor(Color.DARK_GRAY);
		g.fillRect(0, 0, Constants.SCREENW, Constants.SCREENH);

		g.setColor(Color.WHITE);
		g.drawString("FPS: " + frameRate, 10, 10);

		ArrayList<Vec2> corners = new ArrayList<Vec2>();

		g.setColor(blockColor);

		for (MapCorner c : cs) {
			Vec2 mc = state.getCamera().projectWorldToCamera(c.getPos().cpy());
			//System.out.println("corner: " + c + ", toCamera: " + mc);
			
			corners.add(mc);
			//this.drawStateAt(g, "pos: " + c, c.getPos());
			// System.out.printf("Adding: %s%n", c.getPos());
		}

		for (int i = 0; i < corners.size(); i++) {
			if ((i + 1) % 4 == 0) {
				this.drawLine(g, corners.get(i), corners.get(i - 3));
				continue;
			}
			this.drawLine(g, corners.get(i).cpy(), corners.get(i + 1).cpy());
			// System.out.printf("Drawing: %s, %s%n", s, e);

		}

		state.getPlayer();
		state.getPlayer();
		GameDisplay.setPlayerScreenPosition(
				(state.getPlayer().getPos().cpy().sub(state.getCamera().getPos()).getX() + Constants.HALF_SCREENW)
						- (Player.ENT_WIDTH / 2),
				(state.getPlayer().getPos().cpy().sub(state.getCamera().getPos()).getY() + Constants.HALF_SCREENH)
						- (Player.ENT_HEIGHT / 2));
		this.drawPlayer(g);

		// System.out.println("playerPos: " + playerPos);
		g.setColor(Color.WHITE);
		this.drawState(g, "Player pos: " + state.getPlayer().getPos());

		g.setColor(playerPointer);
		Vec2 start = GameDisplay.getPlayerScreenPosition().cpy().add(Entity.ENT_WIDTH / 2, Entity.ENT_HEIGHT / 2);
		Vec2 end = state.getPlayer().getDir().cpy().scl(20).add(start);
		this.drawLine(g, start, end);

		g.setColor(playerColor);
		this.drawPlayer(g);

	}

	private void drawLine(Graphics g, Vec2 start, Vec2 end) {
		g.drawLine((int) start.getX(), (int) start.getY(), (int) end.getX(), (int) end.getY());
	}

	private void drawState(Graphics g, String text) {

		g.setColor(Color.WHITE);
		g.drawString(text,
				(int) (state.getPlayer().getPos().cpy().sub(state.getCamera().getPos()).getX() + Constants.HALF_SCREENW)
						- (Player.ENT_WIDTH / 2) + 40,
				(int) (state.getPlayer().getPos().cpy().sub(state.getCamera().getPos()).getY() + Constants.HALF_SCREENH
						+ 40));
	}

	private void drawStateAt(Graphics g, String text, Vec2 position) {
		Vec2 p = state.getCamera().projectWorldToCamera(position.cpy());
		Color c = g.getColor();
		g.setColor(Color.WHITE);
		g.drawString(text, (int) (p.getX()) + 10, (int) (p.getY()) + 10);
		g.setColor(c);
	}

	private void drawPlayer(Graphics g) {
		g.fillOval((int) GameDisplay.getPlayerScreenPosition().getX(),
				(int) GameDisplay.getPlayerScreenPosition().getY(), Player.ENT_WIDTH, Player.ENT_HEIGHT);
	}

	private void drawRect(Vec2 v1, Vec2 v2, Vec2 v3, Vec2 v4) {
		// Vec2 s = new Vec2(corners.get(i).getX() + Constants.HALF_SCREENW,
		// corners.get(i).getY() + Constants.HALF_SCREENH);
		// Vec2 e = new Vec2(corners.get(i + 1).getX() + Constants.HALF_SCREENW,
		// corners.get(i + 1).getY() + Constants.HALF_SCREENH);

	}

	public static Vec2 getPlayerScreenPosition() {
		return playerScreenPosition;
	}

	private static void setPlayerScreenPosition(float x, float y) {
		playerScreenPosition.set(x, y);
	}

}
