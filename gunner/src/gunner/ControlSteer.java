package gunner;

import gunner.bases.Control;
import gunner.bases.Entity;
import gunner.utils.Vec2;

public class ControlSteer extends Control {

	public ControlSteer (Input input, Entity ent) {
		super(input, ent);
		// TODO Auto-generated constructor stub
	}

	private Vec2 forceToApply = new Vec2();
	
	
	public void updateInputForces () {
		//add to the vel if key is down
		//change
        forceToApply.setX(0);
        forceToApply.setY(0);
        
        super.getEnt().setVel(super.getEnt().getVel().scl(super.getEnt().ENT_NEG_ACC));
        
        super.getEnt().setDir(super.getEnt().getDir().rotate(input.getMouseDeltaX()).nor());
        
        
        
        input.setMouseDeltaX(0);
        
        if (super.input.isPressed(Action.MOVE_UP)) {
        	forceToApply.add(super.getEnt().getDir().cpy().scl(super.getEnt().ENT_ACC));        	
        } 
        
        if (super.input.isPressed(Action.MOVE_DOWN)) {
        	forceToApply.add(super.getEnt().getDir().cpy().scl(-super.getEnt().ENT_ACC));        
        } 
 
        
        if (super.input.isPressed(Action.MOVE_LEFT)) {
        	forceToApply.add(super.getEnt().getDir().cpy().rotate(90).scl(-super.getEnt().ENT_ACC));     
        }
 
        if (super.input.isPressed(Action.MOVE_RIGHT)) {
        	forceToApply.add(super.getEnt().getDir().cpy().rotate(90).scl(super.getEnt().ENT_ACC));     
        }
        
      
        
        
        super.getEnt().setVel(super.getEnt().getVel().add(forceToApply.cpy().scl(0.1f)));
        
      
       /* 
       if (forceToApply.isNil()) {
           super.getEnt().getVel().setX((super.getEnt().getVel().getX() * 0.6f));          
           super.getEnt().getVel().setY((super.getEnt().getVel().getY() * 0.6f));          
       } else {    
           
            if (super.getEnt().getVel().getX() > super.getEnt().ENT_MAX_SPEED) {
               super.getEnt().getVel().setX(super.getEnt().ENT_MAX_SPEED);
           } else if (super.getEnt().getVel().getX() < -super.getEnt().ENT_MAX_SPEED) {
               super.getEnt().getVel().setX(-super.getEnt().ENT_MAX_SPEED);
           }
            if (super.getEnt().getVel().getY() > super.getEnt().ENT_MAX_SPEED) {
                super.getEnt().getVel().setY(super.getEnt().ENT_MAX_SPEED);
            } else if (super.getEnt().getVel().getY() < -super.getEnt().ENT_MAX_SPEED) {
                super.getEnt().getVel().setY(-super.getEnt().ENT_MAX_SPEED);
            }
       }  */    
	}
}
