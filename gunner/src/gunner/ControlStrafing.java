package gunner;

import gunner.bases.Control;
import gunner.bases.Entity;
import gunner.utils.Vec2;

public class ControlStrafing  extends Control {
	
	public ControlStrafing(Input input, Entity ent) {
		super(input, ent);
	}


	private Vec2 forceToApply = new Vec2();
	public void updateInputForces () {
		//add to the vel if key is down
		
        forceToApply.setX(0);
        forceToApply.setY(0);
        
        //get the current direction the ent is facing
        Vec2 dir = super.getEnt().getDir().cpy().nor();
        Vec2 leftVector = new Vec2(-dir.getY(), dir.getX());
        Vec2 rightVector = new Vec2(dir.getY(), -dir.getX());
        
        //get left and right vectors based on this 
        
        
        
        
        if (super.input.isPressed(Action.MOVE_UP)) {
            forceToApply.add(dir.cpy().scl(super.getEnt().ENT_ACC));
        }
        if (super.input.isPressed(Action.MOVE_DOWN)) {
        	 forceToApply.add(dir.cpy().scl(-super.getEnt().ENT_ACC));
        }
 
        if (super.input.isPressed(Action.MOVE_LEFT)) {
        	forceToApply.add(leftVector.scl(-super.getEnt().ENT_ACC));
        }
 
        if (super.input.isPressed(Action.MOVE_RIGHT)) {
        	forceToApply.add(rightVector.scl(-super.getEnt().ENT_ACC));

        }
        
       if (forceToApply.isNil()) {
           super.getEnt().setVel(super.getEnt().getVel().scl(0.95f));          
                     
       } else {
    
    	   
    	   if (forceToApply.len2() > super.getEnt().ENT_MAX_SPEED) {
    		   forceToApply.clamp(super.getEnt().ENT_MAX_SPEED, super.getEnt().ENT_MAX_SPEED);
    	   }
    	   
           super.getEnt().setVel(super.getEnt().getVel().add(forceToApply));
           
           
       }
	}
}
