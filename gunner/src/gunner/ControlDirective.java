package gunner;

import gunner.bases.Control;
import gunner.bases.Entity;
import gunner.utils.Vec2;

public class ControlDirective extends Control {

	public ControlDirective(Input input, Entity ent) {
		super(input, ent);
		// TODO Auto-generated constructor stub
	}

	private Vec2 forceToApply = new Vec2();
	private float maxVel = 0;
	
	public void updateInputForces () {
		//add to the vel if key is down
		
        forceToApply.set(0, 0);
        
        super.getEnt().setDir(GameDisplay.getPlayerScreenPosition().dir(input.getMouseX(), input.getMouseY()).nor());
        
        
        if (super.input.isPressed(Action.GUN_FIRE)) {
            // System.out.println("Mouse 1 down..");
        }
        if (super.input.isPressed(Action.GUN_RELOAD)) {
            // System.out.println("Mouse 1 down..");
        }
         
        
        //get direction of forces
        if (super.input.isPressed(Action.MOVE_UP)) {
            forceToApply.add(0, -1);
        }
        if (super.input.isPressed(Action.MOVE_DOWN)) {
            forceToApply.add(0, 1);
        } 
        if (super.input.isPressed(Action.MOVE_LEFT)) {
           forceToApply.add(-1, 0);
        } 
        if (super.input.isPressed(Action.MOVE_RIGHT)) {
           forceToApply.add(1, 0);
        }
        
        
        //scale direction by acceleration
        forceToApply.nor().scl(super.getEnt().ENT_ACC);      
        super.getEnt().setVel(super.getEnt().getVel().add(forceToApply));
        
       
    	super.getEnt().getVel().scl(0.8f);
        
  
       //determine max vel acheievable 
       if (super.input.isPressed(Action.MOVE_RUN)) {
    	   maxVel = super.getEnt().ENT_MAX_RUN_SPEED;           
       } else {
    	   maxVel = super.getEnt().ENT_MAX_SPEED;
       }
       
       
       //clamp the vel 
       super.getEnt().getVel().clamp(0, maxVel);
       
       
	}

}
