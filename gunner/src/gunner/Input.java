package gunner;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author SAC248
 */
public class Input implements MouseListener, MouseMotionListener, KeyListener {

	private HashMap<Integer, Boolean> pressed = new HashMap<>();
	private int mouseX = 0;
	private int mouseY = 0;

	private int mousePreviousX = 0;
	private int mousePreviousY = 0;

	private int mouseDeltaX = 0;
	private int mouseDeltaY = 0;

	public boolean isPressed(Action action) {
		if (pressed.containsKey(action.getCode())) {
			boolean pressedValue = pressed.get(action.getCode());
			if (pressedValue) {
				System.out.println("Action: " + action + ", is: " + pressed.get(action.getCode()));
			}
			return pressed.get(action.getCode());
		} else {
			return false;
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// System.out.println("mouseClicked");
	}

	@Override
	public void mousePressed(MouseEvent e) {
		pressed.put(e.getButton(), true);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		pressed.put(e.getButton(), false);
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// System.out.println("mouseExited");
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// System.out.println("mouseDragged");
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// System.out.println("mouseMoved");
		this.mousePreviousX = this.mouseX;
		this.mousePreviousY = this.mouseY;

		this.mouseX = e.getX();
		this.mouseY = e.getY();

		this.mouseDeltaX = this.mouseX - this.mousePreviousX;
		this.mouseDeltaY = this.mouseY - this.mousePreviousY;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// System.out.println("keyTyped");
	}

	@Override
	public void keyPressed(KeyEvent e) {
		pressed.put(e.getKeyCode(), true);

	}

	@Override
	public void keyReleased(KeyEvent e) {
		pressed.put(e.getKeyCode(), false);
	}

	public int getMouseDeltaY() {
		return mouseDeltaY;
	}

	public void setMouseDeltaY(int mouseDeltaY) {
		this.mouseDeltaY = mouseDeltaY;
	}

	public int getMouseDeltaX() {
		return mouseDeltaX;
	}

	public void setMouseDeltaX(int mouseDeltaX) {
		this.mouseDeltaX = mouseDeltaX;
	}

	public int getMouseX() {
		return this.mouseX;
	}

	public int getMouseY() {
		return this.mouseY;
	}

	public void setMouseX(int mouseX) {
		this.mouseX = mouseX;
	}

	public void setMouseY(int mouseY) {
		this.mouseY = mouseY;
	}

	public int getMousePreviousX() {
		return mousePreviousX;
	}

	public void setMousePreviousX(int mousePreviousX) {
		this.mousePreviousX = mousePreviousX;
	}

	public int getMousePreviousY() {
		return mousePreviousY;
	}

	public void setMousePreviousY(int mousePreviousY) {
		this.mousePreviousY = mousePreviousY;
	}

}
