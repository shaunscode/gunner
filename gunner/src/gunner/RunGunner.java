
package gunner;



import javax.swing.JFrame;
import gunner.Map;
import gunner.bases.Constants;

public class RunGunner {
    //small change
    public static void main(String[] args) {
        
        //SETUP
        GameWindow window = new GameWindow();
       
        Input input = new Input();
        
        window.addMouseListener(input);
        window.addMouseMotionListener(input);
        window.addKeyListener(input);
        
        Player player = new Player(input);
        Map map = new Map();
        GameState state = new GameState();        
        Camera camera = new Camera(input, player);
        
        
        state.setPlayer(player);
        state.setMap(map);
        state.setCamera(camera);
        
        GameDisplay display = new GameDisplay(state);
        
        
        window.setSize(Constants.SCREENW, Constants.SCREENH);
        window.setLocation(10, 10);
        window.setResizable(false);
        window.setTitle(Constants.WINDOW_TITLE);
        window.setContentPane(display);
        window.setVisible(true);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        System.out.println("setup finished");
         if (!state.isReady()) {
            System.out.println("State was not ready! exiting..");
            return;
        }
       
        long frameStart = System.nanoTime(); 
        long frameEnd = 0;	
        float frameDelta = 0;
        float frameAcc = 0;       

        final int UPDATES_PER_SECOND = 60;
        float frameUpdateRate = 1000 / UPDATES_PER_SECOND;
        long frameTick = 0;
        long loopTick = 0;

        
        //running loop
        while (true) {
               loopTick++;
                //update loop
                while (frameAcc > frameUpdateRate) {
                        frameAcc -= frameUpdateRate;
                        frameTick++;
                        
                        player.update();
                        camera.update();
                       // System.out.println("tick: " + frameTick);                   

                }
                display.repaint(loopTick, frameTick);
                
                        
                frameEnd = System.nanoTime();			
                frameDelta = ((float)(frameEnd - frameStart) / 1000000);
                frameAcc += frameDelta;               
                frameStart = frameEnd;
             
        }
        
    }
    
}
