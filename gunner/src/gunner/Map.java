package gunner;

import java.util.ArrayList;

import gunner.things.MapCorner;
import gunner.things.MapEdge;
import gunner.utils.Vec2;

public class Map {
    //private ArrayList<Vec2> corners;
    
	private ArrayList<MapEdge> edges;
	private ArrayList<MapCorner> corners; 
	
    public Map() {
        
        //corners = new ArrayList<Vec2>();
    	 edges = new ArrayList<MapEdge>();
    	 corners = new ArrayList<MapCorner>();
     	
    	
        int numBoxes = (int)(Math.random() * 20) + 10;
        
        int negX = 0, negY = 0, posX = 0, posY = 0;
        
        for (int i = 0; i < numBoxes; i++) {
        	
        	
	        int w = (int)(Math.random() * 300) + 50;	
	        int h = (int)(Math.random() *  220) + 30;
	        
	        //get random positions
	        int x = (int)(Math.random() * 1600) - 800;
	        int y = (int)(Math.random() * 1600) - 800;
	        
	        MapEdge m1 = new MapEdge(new Vec2(x, y), new Vec2(x + w, y));
	        MapEdge m2 = new MapEdge(new Vec2(x + w, y), new Vec2(x + w, y + h));
	        MapEdge m3 = new MapEdge(new Vec2(x + w, y + h), new Vec2(x, y + h));
	        MapEdge m4 = new MapEdge(new Vec2(x, y + h), new Vec2(x, y));
	        
	        edges.add(m1);
	        edges.add(m2);
	        edges.add(m3);
	        edges.add(m4);
		    
	        MapCorner c1 = new MapCorner(m1, m2);
	        MapCorner c2 = new MapCorner(m2, m3);
	        MapCorner c3 = new MapCorner(m3, m4);
	        MapCorner c4 = new MapCorner(m4, m1);
	        
	        corners.add(c1);
	        corners.add(c2);
	        corners.add(c3);
	        corners.add(c4);
	        
	        	        
	        negX = (x < negX? x: negX);
	        negY = (y < negY? y: negY);
	        
	        posX = ((x + w) > posX? (x + w): posX);
	        posY = ((y + h) > posY? (y + h): posY);
	        
	       //System.out.printf("corner added at %d/%d%n", x, y);
	        
        }
        
        MapEdge e1 = new MapEdge(new Vec2(negX, negY), new Vec2(negX, posY));
        MapEdge e2 = new MapEdge(new Vec2(negX, posY), new Vec2(posX, posY));
        MapEdge e3 = new MapEdge(new Vec2(posX, posY), new Vec2(posX, negY));
        MapEdge e4 = new MapEdge(new Vec2(posX, negY), new Vec2(negX, negY));
         
        MapCorner c1 = new MapCorner (e1, e2);
        MapCorner c2 = new MapCorner (e2, e3);
        MapCorner c3 = new MapCorner (e3, e4);
        MapCorner c4= new MapCorner (e4, e1);
        
        corners.add(c1);
        corners.add(c2);
        corners.add(c3);
        corners.add(c4);
        
        edges.add(e1);
        edges.add(e2);
        edges.add(e3);
        edges.add(e4);
         
        
        
                 
        
    }

    public ArrayList<MapEdge> getEdges() {
    	return edges;
    }

	public ArrayList<MapCorner> getCorners() {
		return corners;
	}

    
}
