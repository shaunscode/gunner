package gunner;

public abstract class Weapon {

	private Ammo ammoType = null;
	
	public boolean hitScan = false;
	public float delay = 0;
	public int ammo = 0;
	
	
	private long lastFired = 0;
	
	
	public Weapon (Ammo ammoType) {
		this.ammoType = ammoType;
	}
	
	public Ammo fire () {
		if (lastFired < System.currentTimeMillis() - delay) {
			lastFired = System.currentTimeMillis();
			ammo--;
			return ammoType;
		}
		return null;
	}
	
	
	
}
