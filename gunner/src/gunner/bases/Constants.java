package gunner.bases;

public class Constants {

	
    public static final int SCREENW = 1680;
    public static final int SCREENH = 1050;
    public static final String WINDOW_TITLE = "Top Down";
    
    public static final int HALF_SCREENW = SCREENW / 2;
    public static final int HALF_SCREENH = SCREENH / 2;
	
    public static final float MOUSE_SPRING = 0.5f;
    public static final float CAMERA_SPRING = 4f;
    
}
