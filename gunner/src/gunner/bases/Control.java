package gunner.bases;

import gunner.Input;

public abstract class Control {

	protected Input input;
	private Entity ent;
	
	public Control (Input input, Entity ent) {
		this.input = input;
		this.setEnt(ent);
	}
	public abstract void updateInputForces  ();
	
	public Entity getEnt() {
		return ent;
	}
	public void setEnt(Entity ent) {
		this.ent = ent;
	}
	
	
	
}
