package gunner.bases;

import gunner.utils.Vec2;

/**
 *
 * @author SAC248
 */
public abstract class Entity {
    private Vec2 pos = new Vec2();
    private Vec2 vel = new Vec2();
    private Vec2 dir = new Vec2(1, 0);
    public static int ENT_WIDTH = 16;
    public static int ENT_HEIGHT = 16;
    public static float ENT_ACC = 2f;
    public float ENT_NEG_ACC = 1 / ENT_ACC;
    
    //average male speeds
    //10.5 jogging
    //25.6 running
    // 1:2.5 ratio
    public static float ENT_MAX_SPEED = 4.0f;
    public static float ENT_MAX_RUN_SPEED = 10.0f;
    public float ENT_SCALE = 7.0f;
    
    public float entSpeed = 0;
    
    public Entity () {};
    
    public Entity (float scale) {
        this.ENT_SCALE = scale;
        this.ENT_HEIGHT *= scale;
        this.ENT_WIDTH *= scale;
        this.ENT_ACC *= (1 / scale);
        this.ENT_MAX_SPEED *= (1 / scale);
        
    }

    public Vec2 getPos() {
        return pos;
    }

    public void setPos(Vec2 pos) {
        this.pos.setX(pos.getX());
        this.pos.setY(pos.getY());
    }

   
    public Vec2 getDir() {
        return dir;
    }

    public void setDir(Vec2 dir) {
        this.dir = dir;
    }
    
    public Vec2 getVel() {
        return vel;
    }

    public void setVel(Vec2 vel) {
        this.vel = vel;
    }
    
    public void applyForce (Vec2 force) {
        this.vel.add(force);
    }
    
    public void applyXForce(float force) {
        this.vel.add(force, 0);
    }
    
    public void applyYForce(float force) {
        this.vel.add(0, force);
    }
   
}
