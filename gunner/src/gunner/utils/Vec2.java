package gunner.utils;


public class Vec2 {
    
	private int id;
	private int idCounter;
	
    private float x = 0;
    private float y= 0;
    
    public Vec2 () {};
    
    public Vec2 (float x, float y) {
        this.x = x;
        this.y = y;
        
        this.id = idCounter++;
        
    }
    
    public int getId () {
    	return this.id;
    }
    
    
    public Vec2 (Vec2 v) {
        this(v.x, v.y);
    }
    
    public boolean equals (Vec2 v) {
    	if (this.x == v.x && this.y == v.y) {
    		return true;
    	}
    	return false;
    }
    
    public float getX () {
        return this.x;
    }

    public float getY () {
        return this.y;
    }
    
    public Vec2 setX (float x) {
    	this.x = x;
    	return this;
    }
    
    public Vec2 setY (float y) {
    	this.y = y;
    	return this;
    }
    
    public Vec2 set (float x, float y) {
      this.x = x;
      this.y = y;
      return this;
    }
    
    public Vec2 set (Vec2 v) {
        return this.set(v.x, v.y);
    }
    
    public Vec2 add (float x, float y) {
        this.x += x;
        this.y += y;
        return this;
    }
    
    public Vec2 add (Vec2 v) {
        return this.add(v.x, v.y);
    }
    
    
    public Vec2 sub (float x, float y) {
        this.x -= x;
        this.y -=y;
        return this;
    }
    
    public Vec2 sub (Vec2 v) {
        return this.sub(v.x, v.y);
    }
    
    public float len () {
    	return (float) Math.sqrt((this.x * this.x) + (this.y * this.y));
    }
    public float len2 () {
    	return ((this.x * this.x) + (this.y * this.y));
    }
    
    public Vec2 scl (float by) {
    	
        this.x *= by;
        this.y *= by;
       // System.out.println("scl by: " + by + ", x/y now equal: " + x + "/" + y);
        return this;
    }
    
    public Vec2 nor () {
        float length = len();
        if (length != 0) {
    		this.x = (float) (this.x / length);
    		this.y = (float) (this.y / length);
        }
        return this;
    }
    
    public Vec2 dir (float x, float y) {
        Vec2 dir = new Vec2();
        dir.x = x - this.x;
        dir.y = y - this.y;
        return dir;
    }
    
     
	public Vec2 clamp (float min, float max) {
		final float len2 = len2();
		if (len2 == 0f) return this;
		float max2 = max * max;
		if (len2 > max2) return scl((float)Math.sqrt(max2 / len2));
		float min2 = min * min;
		if (len2 < min2) return scl((float)Math.sqrt(min2 / len2));
		return this;
	}
    
    public Vec2 dir (Vec2 v) {
        return this.dir(v.x, v.y);
    }
    
    public Vec2 cpy () {
        return new Vec2(this.x, this.y);
    }
    
    public boolean isNil () {
        return (this.x == 0 && this.y == 0);
    }
    
    public boolean isXNil () {
        return this.x == 0;
    }
    
    public boolean isYNil () {
        return this.y == 0;
    }
    
    public String toString () {
        return "[" + this.x + ", " + this.y + "]";
    }    
    
    public Vec2 rotate (float degrees) {
		return rotateRad(degrees * (float) (Math.PI / 180));
	}

	/** Rotates the Vector2 by the given angle, counter-clockwise assuming the y-axis points up.
	 * @param radians the angle in radians */
	public Vec2 rotateRad (float radians) {
		float cos = (float)Math.cos(radians);
		float sin = (float)Math.sin(radians);

		float newX = this.x * cos - this.y * sin;
		float newY = this.x * sin + this.y * cos;

		this.x = newX;
		this.y = newY;

		return this;
	}
    
    
    
    
    
    
    
    
    
}