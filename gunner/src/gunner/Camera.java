package gunner;

import gunner.bases.Constants;
import gunner.bases.Control;
import gunner.bases.Entity;
import gunner.utils.Vec2;

public class Camera extends Entity {

	private Control control;
	private Input input;
	private float zoom;
	
	
	public Camera (Input input, Entity player) {
		this.input = input;
		this.control = new ControlCamera(input, this, player);
		this.zoom = 1.0f;
	}
		
	public void focus (Vec2 on) {

		super.setPos(on);
	
	}
	
	public void update () {
		control.updateInputForces();
	}

	public float getZoom() {
		return zoom;
	}

	public void setZoom(float zoom) {
		this.zoom = zoom;
	}
	
	public Vec2 projectWorldToCamera (Vec2 v) {
		v.add(getPos().cpy().scl(-1));
		v.add( + Constants.HALF_SCREENW,  + Constants.HALF_SCREENH);
		return v;
	}
}
