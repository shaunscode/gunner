package gunner.things;

import gunner.utils.Vec2;

public class MapCorner {

	private Vec2 pos = new Vec2();
	private Vec2 nor = new Vec2();
	
		
	public MapCorner (MapEdge m1, MapEdge m2) {
		
		if (m1.getEnd().equals(m2.getStart())) {
			System.out.println("start/end are equal");
			this.pos = m2.getEnd().cpy();
		}
		
		//Vec2 e1 = m1.getEnd().cpy().sub(m1.getStart());
		//Vec2 e2 = m2.getEnd().cpy().sub(m2.getStart());
		
		//nor.set(e1.cpy().add(e2).scl(0.5f).rotate(90).nor());
		
	}


	public Vec2 getPos() {
		return pos;
	}


	public void setPos(Vec2 pos) {
		this.pos = pos;
	}


	public Vec2 getNor() {
		return nor;
	}


	public void setNor(Vec2 nor) {
		this.nor = nor;
	}
	
	public String toString ( ) {
		return this.pos.toString();
	}
	
}
