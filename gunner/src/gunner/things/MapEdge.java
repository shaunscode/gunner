package gunner.things;

import gunner.utils.Vec2;

public class MapEdge {

	private Vec2 start;
	private Vec2 middle;
	private Vec2 end;
	private Vec2 normal;
	private Vec2 startNormal;
	private Vec2 endNormal;
	
	public MapEdge (Vec2 start, Vec2 end) {
		this.start = start;
		this.end = end;
		Vec2 points = end.cpy().sub(start);
		middle = points.cpy().scl(0.5f);
		
		this.normal = points.rotate(-90).nor();
	}

	public Vec2 getStart() {
		return start;
	}

	public void setStart(Vec2 start) {
		this.start = start;
	}

	public Vec2 getMiddle() {
		return middle;
	}

	public void setMiddle(Vec2 middle) {
		this.middle = middle;
	}

	public Vec2 getEnd() {
		return end;
	}

	public void setEnd(Vec2 end) {
		this.end = end;
	}

	public Vec2 getNormal() {
		return normal;
	}

	public void setNormal(Vec2 normal) {
		this.normal = normal;
	}

	public Vec2 getStartNormal() {
		return startNormal;
	}

	public void setStartNormal(Vec2 startNormal) {
		this.startNormal = startNormal;
	}

	public Vec2 getEndNormal() {
		return endNormal;
	}

	public void setEndNormal(Vec2 endNormal) {
		this.endNormal = endNormal;
	}
	
}
