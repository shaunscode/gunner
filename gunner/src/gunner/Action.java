package gunner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

/**
 *
 * @author SAC248
 */
 public enum Action {
		 MOVE_CAMERA_UP(KeyEvent.VK_NUMPAD8, "Camera Up"),
		 MOVE_CAMERA_DOWN(KeyEvent.VK_NUMPAD2, "Camera Down"),
		 MOVE_CAMERA_LEFT(KeyEvent.VK_NUMPAD4, "Camera Left"),
		 MOVE_CAMERA_RIGHT(KeyEvent.VK_NUMPAD6, "Camera Right"),
	 
		 B(KeyEvent.VK_B, "B"),
		 
        MOVE_UP(KeyEvent.VK_W, "Move Up"), 
        MOVE_DOWN(KeyEvent.VK_S, "Move Down"), 
        MOVE_LEFT(KeyEvent.VK_A, "Move Left"),
        MOVE_RIGHT(KeyEvent.VK_D, "Move Right"),
        MOVE_RUN(KeyEvent.VK_SHIFT, "Move Run"),
        
        GUN_FIRE(MouseEvent.BUTTON1, "Gun Fire"),
        GUN_RELOAD(MouseEvent.BUTTON3, "Gun Reload"),
        GUN_CHANGE_UP(0, "Gun Change Up"),
        GUN_CHANGE_DOWN(0, "Gun Change Down");
    
        private int actionCode;
        private String description; 
        
        private Action (int actionCode, String desc) {
            this.actionCode = actionCode;
            this.description = desc;
        }
        
        public int getCode () {
            return actionCode;
        }
        
        public void setCode (int actionCode) {
            this.actionCode = actionCode;
        }
            
        public Action getActionByCode (int code) {
            Action[] actions = Action.values();
            for (int i = 0; i < actions.length; i++) {
                if (actions[i].getCode() == code) {
                    return actions[i];
                }
            }
            return null;
        
        }
        
        public String toString () {
        	return this.description;
        }
        
        
    };