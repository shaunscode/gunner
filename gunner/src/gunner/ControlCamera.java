package gunner;

import gunner.bases.Constants;
import gunner.bases.Control;
import gunner.bases.Entity;
import gunner.utils.Vec2;

public class ControlCamera extends Control {

	private Entity player = null;
	
	public ControlCamera(Input input, Entity ent, Entity player) {
		super(input, ent);
		this.player = player;
	}

	private Vec2 forceToApply = new Vec2();
	private Vec2 camVec = new Vec2();
	public void updateInputForces () {		
				
		//get position of mouse relative to centre of screen
		int mouseX = input.getMousePreviousX();
		int mouseY = input.getMousePreviousY();
		
		camVec.set(mouseX - Constants.HALF_SCREENW, mouseY - Constants.HALF_SCREENH);
		camVec.set(camVec.scl(Constants.MOUSE_SPRING));
		//System.out.println("camVec x/y: " + camVec.getX() + "/" + camVec.getY());
		
		//shift camera position in direction of mouse pos to center of screen
		//set camera position to player position
		forceToApply.set(player.getPos().cpy().add(camVec).add(player.getVel().cpy().scl(Constants.CAMERA_SPRING)));
		//System.out.println("newPos: " + newPos);
		super.getEnt().setPos(forceToApply);
				
		
	}
}
