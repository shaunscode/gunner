package gunner;

import gunner.bases.Control;
import gunner.bases.Entity;
import gunner.utils.Vec2;

public class Player extends Entity {
    
    private Input input;
    private Control control;
    
    //private Vec2 screenPos = new Vec2();
        
    public Player (Input input) {
        this.input = input;
        this.getPos().set(0, 0);
        this.control = new ControlDirective(input, this);
    }
    
   
    public void update  () {
       
    	control.updateInputForces();
    	
        super.setPos(super.getPos().add(super.getVel()));
       
    }
    
}
