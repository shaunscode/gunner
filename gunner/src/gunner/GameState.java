package gunner;


import java.util.ArrayList;

import gunner.Map;
import gunner.bases.Entity;


/**
 *
 * @author SAC248
 */
public class GameState {
    
    private ArrayList<Entity> ents;
    private Player player;
    private Camera camera;
    private Map map;
    
    public GameState () {
        this.ents = new ArrayList<Entity>();
    }

    public void setMap (Map map) {
        this.map = map;
    }
    
    public Map getMap () {
        return this.map;
    }
    
    public void setPlayer (Player player) {
        this.player = player;
    }
    
    public Player getPlayer () {
        return this.player;
    }

    public ArrayList<Entity> getEnts() {
        return ents;
    }

    public void setEnts(ArrayList<Entity> ents) {
        this.ents = ents;
    }

    public boolean isReady () {
        return (map != null && player != null);
    }

    public Camera getCamera() {
		return camera;
	}

    public void setCamera(Camera camera) {
		this.camera = camera;
	}
    
 
}
