# README #

This repo is for storing my simple game that is used as a test dummy for anything really. Pretty much a 2d quake clone with weapons and armour is the goal.

currently implemented..

* mouse look controls with view movement
* key binding/mapping setup
* running also shifts view
* maps randomly generated (as simple as they are)

future goals..

* network multiplayer! the goal that started it all...
* weapons/armour
* ai bots
* seed for map generation
* BSP tree the maps for potential visible sets
* zoom on run, 2d implementation of gears of war/ghost recon forced focal point 
 
Using all java without frame works, so can get the source and run easily with no dependancies.